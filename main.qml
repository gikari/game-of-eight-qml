import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    visible: true
    width: 640
    height: 640
    title: qsTr("Game of Eight")

    function moveToState(currentState, nextState) {

    }

    function isRepeatedState(state, visitedStates) {
        return visitedStates.includes(state)
    }

    function isWin() {
        return numbers.get(0).value === 1
          && numbers.get(1).value === 2
          && numbers.get(2).value === 3
          && numbers.get(3).value === 4
          && numbers.get(4).value === 5
          && numbers.get(5).value === 6
          && numbers.get(6).value === 7
          && numbers.get(7).value === 8
    }

    function possibleMoves() {
        const possibleDirections = []
        for (const direction = 0; direction < 4; direction++) {
            if (getCellindexFrom(direction) !== -1) {
                possibleDirections.push(direction)
            }
        }
        return possibleDirections
    }

    function scramble() {
        const untakenCells = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        for (let elementNumber = 0; elementNumber < 9; elementNumber++) {
            const randomIndex = Math.floor(Math.random() * untakenCells.length)
            const randomPlace = untakenCells[randomIndex]
            numbers.set(randomPlace, { value: elementNumber })
            untakenCells.splice(randomIndex, 1)
        }
    }

    function swapNumbers(first, second) {
        const tmp = numbers.get(first).value
        numbers.set(first, {value: numbers.get(second).value})
        numbers.set(second, {value: tmp})
    }

    function getEmptyCellIndex() {
        for (let i = 0; i < numbers.count; i++) {
            if (numbers.get(i).value === 0) {
                return i;
            }
        }
        return -1;
    }

    // 0 - Up, 1 - Right, 2 - Down, 3 - Left
    function getCellindexFrom(direction) {
        const emptyCellPosition = getEmptyCellIndex()

        let neighbourCellIndex = -1
        if (direction === 0) {
            neighbourCellIndex = emptyCellPosition - 3
        } else if (direction === 1) {
            if (emptyCellPosition !== 2 && emptyCellPosition !== 5) {
                neighbourCellIndex = emptyCellPosition + 1
            }
        } else if (direction === 2) {
            neighbourCellIndex = emptyCellPosition + 3
        } else if (direction === 3) {
            if (emptyCellPosition !== 3 && emptyCellPosition !== 6) {
                neighbourCellIndex = emptyCellPosition - 1
            }
        }

        if (neighbourCellIndex < 0 || neighbourCellIndex >= numbers.count) {
            return -1
        } else {
            return neighbourCellIndex
        }
    }

    function inverseDirection(direction) {
        return (direction + 2) % 4
    }

    function moveCellFrom(direction) {
        const neighbourCellIndex = getCellindexFrom(inverseDirection(direction))

        if (neighbourCellIndex === -1) {
            return -1
        }

        const emptyCellIndex = getEmptyCellIndex()

        swapNumbers(neighbourCellIndex, emptyCellIndex)
    }

    Rectangle {
        anchors.fill: parent
        border {
            color: "darkgrey"
            width: 5
        }

        GridView {
            id: grid
            anchors.margins: 5
            anchors.fill: parent
            cellWidth: width / 3
            cellHeight: height / 3

            focus: true
            Keys.onPressed: {
                if (event.key === Qt.Key_Up) {
                    moveCellFrom(0)
                } else if (event.key === Qt.Key_Right) {
                    moveCellFrom(1)
                } else if (event.key === Qt.Key_Down) {
                    moveCellFrom(2)
                } else if (event.key === Qt.Key_Left) {
                    moveCellFrom(3)
                } else if (event.key === Qt.Key_R) {
                    scramble()
                }
            }

            model: ListModel {
                id: numbers
                ListElement {
                    value: 8
                }
                ListElement {
                    value: 7
                }
                ListElement {
                    value: 3
                }
                ListElement {
                    value: 1
                }
                ListElement {
                    value: 5
                }
                ListElement {
                    value: 6
                }
                ListElement {
                    value: 4
                }
                ListElement {
                    value: 2
                }
                ListElement {
                    value: 0
                }
            }
            delegate: Rectangle {
                width: grid.width / 3
                height: grid.height / 3
                color: model.value !== 0 ? "grey" : "lightgrey"
                border {
                    width: 5
                    color: "darkgrey"
                }
                Text {
                    id: name
                    anchors.centerIn: parent
                    text: model.value !== 0 ? value : ""

                    color: "lightgrey"
                    font.weight: Font.Bold
                    font.pixelSize: parent.width / 3
                }
            }
        }

        Rectangle {
            anchors.fill: parent
            color: "#AAFFFFFF"
            visible: isWin()
            Text {
                text: "Press R to restart"
                anchors.centerIn: parent
                font.pixelSize: parent.width / 10
            }
        }



    }
}
