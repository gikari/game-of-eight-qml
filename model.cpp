#include "model.h"
#include <QDebug>

Model::Model(QObject *parent) : QObject(parent), currentState{{8, 7, 3}, {1, 5, 6}, {4, 2, 0}}, visitedStates{}, statesToAnalyze{}
{

}

void Model::solve()
{
    solveWithBreadthSearch();
}

bool Model::isRepeatedState(GameState state)
{
    return visitedStates.contains(state);
}

char Model::getCellChar(int row, int column)
{
    int cellValue {-1};
    try {
        cellValue = currentState[row][column];
        return cellValue == 0 ? ' ' : QString::number(cellValue).toStdString()[0];
    } catch (std::exception) {
        return 'X';
    }
}

void Model::printCurrentState()
{
    qDebug() << getCellChar(0, 0) << "|" << getCellChar(0, 1) << "|" << getCellChar(0, 2) << "\n"
             << "-+-+-"
             << getCellChar(1, 0) << "|" << getCellChar(1, 1) << "|" << getCellChar(1, 2) << "\n"
             << "-+-+-"
             << getCellChar(2, 0) << "|" << getCellChar(2, 1) << "|" << getCellChar(2, 2) << "\n" ;
}

void Model::solveWithBreadthSearch()
{
    if (isWin()) {
       qDebug() << "WIN";
    } else {
        printCurrentState();
        if (!isRepeatedState(currentState)) {
           visitedStates += currentState;
           statesToAnalyze += getPossibleFutureStates();
           currentState = statesToAnalyze[0];
           statesToAnalyze.pop_front();
        } else {
            qDebug() << "REPEATED STATE";
        }
    }
}

QVector<GameState> Model::getPossibleFutureStates()
{
    QVector<GameState> futureStates {};

    CellAddress zeroCellCoordinates = getZeroCellAddress();
    QVector<Direction> directions {Direction::UP, Direction::RIGHT, Direction::DOWN, Direction::LEFT};
    for (Direction direction : directions) {
        if (getNeighboor(zeroCellCoordinates, direction) != -1) {
            futureStates += makeMoveInCurrentState(direction);
        }
    }
    return futureStates;
}

bool Model::isWin()
{
    return currentState[0][0] == 1
        && currentState[0][1] == 2
        && currentState[0][2] == 3
        && currentState[1][0] == 4
        && currentState[1][1] == 5
        && currentState[1][2] == 6
        && currentState[2][0] == 7
            && currentState[2][1] == 8;
}

CellAddress Model::getZeroCellAddress()
{
    for (int i = 0; i < currentState.length(); i++) {
        for (int j = 0; j < currentState[i].length(); j++) {
            if (currentState[i][j] == 0) {
                return {i, j};
            }
        }
    }
    return {-1, -1};
}

int Model::getNeighboor(CellAddress cellAddress, Model::Direction direction)
{
    try {
        CellAddress neighboorAddress {getNeighboorAddress(cellAddress, direction)};
        return currentState[neighboorAddress.first][neighboorAddress.second];
    } catch (std::exception) {
        return -1;
    }
}

CellAddress Model::getNeighboorAddress(CellAddress cellAddress, Model::Direction direction)
{
    int rowNumber = cellAddress.first;
    int columnNumber = cellAddress.second;
    switch (direction) {
        case Direction::UP:
            return {rowNumber - 1, columnNumber};
        case Direction::RIGHT:
            return {rowNumber, columnNumber + 1};
        case Direction::DOWN:
            return {rowNumber + 1, columnNumber};
        case Direction::LEFT:
            return {rowNumber, columnNumber - 1};
    }
    return {-1, -1};
}

GameState Model::makeMoveInCurrentState(Model::Direction direction)
{
    CellAddress zeroCellAddress = getZeroCellAddress();
    int neighboorValue {getNeighboor(zeroCellAddress, direction)};
    if (neighboorValue == -1) {
        return {};
    } else {
        GameState futureState = currentState;
        CellAddress neighboorAddress = getNeighboorAddress(zeroCellAddress, direction);
        int tmp = futureState[zeroCellAddress.first][zeroCellAddress.second];
        futureState[zeroCellAddress.first][zeroCellAddress.second] = neighboorValue;
        futureState[neighboorAddress.first][neighboorAddress.second] = tmp;
        return futureState;
    }
}


