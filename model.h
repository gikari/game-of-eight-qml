#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QVector>
#include <QSet>

using GameState = QVector<QVector<int>>;
using CellAddress = std::pair<int, int>;
class Model : public QObject
{
    Q_OBJECT
public:
    enum class Direction {UP, RIGHT, DOWN, LEFT};

    explicit Model(QObject *parent = nullptr);

signals:

public slots:

public:
    void solve();
    bool isRepeatedState(GameState state);
private:
    char getCellChar(int row, int column);
    void printCurrentState();

    bool isWin();
    CellAddress getZeroCellAddress();
    void solveWithBreadthSearch();
    QVector<GameState> getPossibleFutureStates();

    int getNeighboor(CellAddress cellAddress, Direction direction);
    CellAddress getNeighboorAddress(CellAddress cellAddress, Direction direction);
    GameState makeMoveInCurrentState(Direction direction);

    GameState currentState;
    QSet<GameState> visitedStates;
    QVector<GameState> statesToAnalyze;
};

#endif // MODEL_H
